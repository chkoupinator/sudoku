## Installation & Usage
You simply need a working Python 3.6+ installation (Download Link: https://www.python.org/downloads/ )

Simply run the game using `python main.py` and follow the written instructions

## Features
- Fast grid generation that still ensures only one solution exists
- Customizable CLI input handling
- Time tracking for the *competitive* Sudoku players
