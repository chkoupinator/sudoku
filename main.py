from sudoku import SudokuGame

try:
    game = SudokuGame()
    game.start_game()
except KeyboardInterrupt:
    print("\nGoodbye then!")
