import random
import time
from abc import ABC, abstractproperty


class Position:
    def __init__(self, row, col):
        self.row = row
        self.col = col


class SudokuCellsContainer(ABC):
    def __init__(self, pos):
        self.pos = pos
        self.line = None

    @property
    def assigned(self):
        return sum(self.line)


class GenerationError(Exception):
    def __init__(self, board, item):
        self.board = board  # type: SudokuBoard
        self.item = item    # type: SudokuCell or SudokuRow or SudokuColumn

    def __str__(self):
        return f"{repr(self.item)} was not able to receive any value during generation"


class SolutionNotUnique(Exception):
    pass


def format_time(t: float):
    h = int(t // 3600)
    m = int((t % 3600) // 60)
    s = int(t % 60)
    if h:
        return f"{h:}h {m:02}m {s:02}s"
    if m:
        return f"{m}m {s:02}s"
    return f"{s} seconds"


class SudokuCell:
    def __init__(self, pos: Position, board):
        self.value = 0
        self.lock = False
        self.selected = False
        self.pos = pos
        self.board = board      # type: SudokuBoard
        self.subgrid = None     # type: SudokuSubGrid
        self.row = None         # type: SudokuRow
        self.column = None      # type: SudokuColumn

    def assign_static_value(self, value):
        self.value = value
        self.lock = True
        return value

    def unassign_static_value(self):
        v = self.value
        self.value = 0
        self.lock = False
        return v

    def assign_value(self, value):
        if self.lock:
            return False
        self.value = value
        return True

    def clear_value(self):
        if self.lock:
            return False
        self.value = 0

    def can_have_value(self, value):
        return value not in self.subgrid and value not in self.row and value not in self.column

    def has_duplicate_value(self):
        value = self.value
        self.value = 0
        ans = self.can_have_value(value)
        self.value = value
        return not ans

    def random_assign(self, numbers):
        while len(numbers):
            n = numbers.pop(random.randint(0, len(numbers)-1))
            if self.can_have_value(n):
                self.assign_static_value(n)
                return numbers
        return numbers

    def __str__(self):
        return f'{self.value}'

    def __int__(self):
        return self.value

    def __eq__(self, other):
        return int(other) == self.value

    def __add__(self, other):
        return int(other) + int(self.lock)

    def __radd__(self, other):
        return int(other) + int(self.lock)

    def __repr__(self):
        return f'Cell at row {self.pos.row+1} at col {self.pos.col+1}, value: {self.value}, lock: {self.lock}, ' \
               f'selected: {self.selected}'


class SudokuSubGrid(SudokuCellsContainer):
    def __init__(self, board, pos: Position):
        super().__init__(pos)
        subgrid = list()
        self.line = list()
        for row in range(pos.row*3, pos.row*3 + 3):
            subgrid.append(list())
            for col in range(pos.col*3, pos.col*3+3):
                cell = board.grid[row][col]
                cell.subgrid = self
                subgrid[row % 3].append(cell)
        self.grid = subgrid
        for i in range(9):
            self.line.append(subgrid[i//3][i % 3])

    def __contains__(self, item: SudokuCell):
        for row in self.grid:
            if item in row:
                return True
        return False

    def __getitem__(self, item: int):
        return self.line[int(item)]


class SudokuRow(SudokuCellsContainer):
    def __init__(self, board, pos):
        super().__init__(pos)
        self.line = board.grid[pos.row]
        for cell in self.line:
            cell.row = self

    def __contains__(self, item: SudokuCell):
        return item in self.line

    def __getitem__(self, item: int) -> SudokuCell:
        return self.line[int(item)]

    def __repr__(self):
        return f'Row at index {self.pos.row} with values {self.line}'


class SudokuColumn(SudokuCellsContainer):
    def __init__(self, board, pos):
        super().__init__(pos)
        self.line = list(board.grid[row][pos.col] for row in range(9))
        for cell in self.line:
            cell.column = self

    def __contains__(self, item: SudokuCell):
        return item in self.line

    def __getitem__(self, item: int):
        return self.line[int(item)]

    def __repr__(self):
        return f'Column at index {self.pos.col} with values {self.line}'


class SudokuBoard(SudokuCellsContainer):
    def __init__(self, game):
        super().__init__(Position(0, 0))
        self.gaps = None
        self.to_assign = None
        self.line = list()
        self.grid = list()
        self.rows = list()
        self.columns = list()
        self.subgrids = list()
        self.game = game            # type: SudokuGame
        for row in range(9):
            self.grid.append(list())
            for col in range(9):
                cell = SudokuCell(Position(row, col), self)
                self.grid[row].append(cell)
                self.line.append(cell)
        for row in range(3):
            self.subgrids.append(list())
            for col in range(3):
                self.subgrids[row].append(SudokuSubGrid(self, Position(row, col)))
        for row in range(9):
            self.rows.append(SudokuRow(self, Position(row, 0)))
        for col in range(9):
            self.columns.append(SudokuColumn(self, Position(0, col)))

    def __str__(self):
        imeths = self.game.input_method
        space = ' ' if imeths['mode'] in ['N', 'S'] else ''
        s = f"\n {space}"

        for col in range(9):
            if imeths["mode"] == "N":
                s = f'{s} {imeths["index_to_output"]["cols"][col]}  '
            elif imeths["mode"] == "S":
                if col < 3:
                    s = f'{s} {imeths["index_to_output"]["cols"][col]}  '

        s = f"{s}\n{space}------------#-----------#------------\n"
        line_prefix = ''
        line_suffix = ''
        for row in range(9):
            if imeths["mode"] == "N":
                line_prefix = f'{imeths["index_to_output"]["rows"][row]}'
            elif imeths["mode"] == "S":
                if row < 3:
                    line_prefix = f'{imeths["index_to_output"]["rows"][row]}'
                else:
                    line_prefix = ' '
                if not ((row-1) % 3):
                    line_suffix = f'{imeths["index_to_output"]["sg_rows"][row//3]}'
                else:
                    line_suffix = ""
            cells = [[self.grid[row][j*3+i] for i in range(3)] for j in range(3)]
            line = ('|'.join(f'{">" if cell.selected else ("[" if int(cell) and not cell.lock else " ")}'
                             f'{str(cell) if int(cell) != 0 else " "}'
                             f'{"<" if cell.selected else ("]" if int(cell) and not cell.lock else  " ")}'
                             for cell in sg_cells) for sg_cells in cells)
            s = f"{s}{line_prefix}|{'#'.join(line)}|{line_suffix}\n"
            if row < 8:
                if not (row+1) % 3:
                    s = f"{s}{' ' if line_prefix else ''}#####################################\n"
                else:
                    s = f"{s}{' ' if line_prefix else ''}|---+---+---#---+---+---#---+---+---|\n"
        s = f"{s}{' ' if line_prefix else ''}------------#-----------#------------\n{space}"
        if imeths["mode"] == "S":
            for col in range(3):

                s = f'{s}      {imeths["index_to_output"]["sg_cols"][col]}     '
        s = f'{s}\n'
        return s

    def __getitem__(self, item) -> SudokuCell:
        return self.line[int(item)]

    def fill(self, index: int=0):
        if index == 81:
            return True
        numbers = list(range(1, 10))
        while len(numbers):
            numbers = self[index].random_assign(numbers)
            if len(numbers) == 0:
                break
            if self.fill(index+1):
                return True
        self[index].unassign_static_value()
        return False

    def check_solutions(self, index: int=0):  # complexity of 9^n, n being the amount of gaps there is
        #  not really the most interesting, especially to check if there is only one solution
        solutions = 0
        while index < 81 and self[index] != 0:
            index += 1
        if index == 81:
            return 1
        for i in range(1, 10):
            if self[index].can_have_value(i):
                self[index].value = i
                solutions += self.check_solutions(index)
                self[index].value = 0
        return solutions

    # def make_gaps(self):
    #     for row in self.rows:
    #         indexes = list(range(9))
    #         while row.assigned > self.to_assign and len(indexes):
    #             i = indexes.pop(random.randint(0, len(indexes)-1))
    #             if row[i].column.assigned > self.to_assign and row[i].subgrid.assigned > self.to_assign:
    #                 row[i].unassign_static_value()

    def make_gaps(self, index=None):
        solutions = 0

        def check_solutions(i: int = 0):
            nonlocal solutions
            if solutions > 1:
                raise SolutionNotUnique
            while i < 81 and self[i] != 0:
                i += 1
            if i == 81:
                solutions += 1
                return
            for v in range(1, 10):
                if self[i].can_have_value(v):
                    self[i].value = v
                    check_solutions(i)
                    self[i].value = 0
        if index is None:
            index = list(range(81))
        if not index:
            return
        cell = self[index.pop(random.randint(0, len(index)-1))]
        if cell.lock and (cell.subgrid.assigned > self.to_assign and
           cell.row.assigned > self.to_assign and
           cell.column.assigned > self.to_assign):
            value = cell.unassign_static_value()
            try:
                check_solutions()
            except SolutionNotUnique:
                cell.assign_static_value(value)
            self.clear_non_locked_values()

        self.make_gaps(index)

    def clear_non_locked_values(self):
        for i in range(81):
            if not self[i].lock:
                self[i].value = 0

    def has_duplicates(self):
        for i in range(81):
            if self[i].has_duplicate_value():
                return True
        return False

    def has_empty_cells(self):
        for i in range(81):
            if self[i].value == 0:
                return True
        return False


class SudokuGame:
    def __init__(self, board: SudokuBoard =None, difficulty: int = None):
        self.board = board                  # type: SudokuBoard
        self.difficulty = difficulty
        self.selected_cell = None           # type: SudokuCell
        self.time_beginning = None          # type: float
        self.input_method = {
            "mode": None,       # can be either N for Normal, S for Subgrids or T for Tryhard
            "input_to_index": {
                "rows": {},
                "cols": {}
            },     # will contain either:
            # N: 2 lists containing a dict with the mappings of the cell's rows then cols
            # S: 4 lists containing a dict with the mapping of the subgrid's rows then cols then the
            # rows and cols inside the subgrid
            # T: None, Tryhards use the cell's linear index from 0 to 80 directly, like real men
            "index_to_output": {
                "rows": {},
                "cols": {}
            },
            "len_input": 0
        }
        self.track_time = None

    @property
    def time_elapsed(self):
        return format_time(time.time() - self.time_beginning)

    def show_board(self):
        space = ' ' if self.input_method['mode'] in ['N', 'S'] else ''
        s = f"{space}_____________________________________\n\n" \
            f"{space}#### #    # ####   ####  #   # #    #\n" \
            f"{space}#    #    # #   # #    # #  #  #    #\n" \
            f"{space}#### #    # #   # #    # ###   #    #\n" \
            f"{space}   # #    # #   # #    # #  #  #    #\n" \
            f"{space}####  ####  ####   ####  #   #  #### \n"

        t = ""
        if self.track_time and self.time_beginning is not None:
            t = f"Time: {self.time_elapsed}"
        if self.difficulty is not None:
            print(f"\n{s}\n"
                  f"{space}Difficulty: {self.difficulty} {t:>23}\n"
                  f"{space}_____________________________________")
        print(str(self.board))

    def new_board(self):
        self.board = SudokuBoard(self)

    def lock_difficulty(self):
        self.board.gaps = [0, 1, 3, 5, 6, 7][self.difficulty]
        self.board.to_assign = 9 - self.board.gaps
        # from 1 = easiest to 5 = hardest, determines gaps per subsection (row, column, subgrid)

    def make_board_playable(self):
        self.board.fill()
        self.lock_difficulty()
        self.board.make_gaps()

    @staticmethod
    def ask_difficulty(re=False):
        difficulty_levels = "(1 = easiest, 2 = easy, 3 = medium, 4 = hard, 5 = hardest)"
        while True:
            try:
                if re:
                    return int(input(f"Please select the new difficulty {difficulty_levels}: "))
                else:
                    return int(input(f"Please select a difficulty level {difficulty_levels}: "))
            except ValueError:
                print("The value must be a number (between 1 and 5)")

    @staticmethod
    def ask_yesno(string: str):
        while True:
            answ = input(string)
            if answ.lower().startswith("y"):
                return True
            elif answ.lower().startswith("n"):
                return False
            else:
                print("You can only answer this with yes (y works too) or no (n works too)")

    @staticmethod
    def ask_keep_settings():
        answ = input(f"Would you like to keep your settings? (y/n) : ")
        return answ.lower().startswith("y")

    @staticmethod
    def set_input_method():
        while True:
            mode = input("There are 3 input methods available would you rather use:\n"
                         "- Normal mode: where you will give the row and column of each cell (ex: 45, DE or D5 for "
                         "row 4 column 5)\n"
                         "- Subgrids mode: where you will give first the position of the subgrid and then the position "
                         "of the cell within it (ex: BB 12, B2 A2, 22 12 or 11 01 for row 4 col 5)\n"
                         "- Tryhard mode: where you will give one single index number from 0 to 80, the cell "
                         "0 being row 1 col 1, 1 being row 1 col 2 and 80 being row 9 col 9\n").upper()
            if mode[0] not in ["N", "S", "T"]:
                print('The mode can only be one of the three above, type \"S\" or \"subgrid\" for the subgrid mode,'
                      ' "n" or "Normal" for the normal mode and "T" or "TRYHARD" for the tryhard mode.')
            else:
                break
        quick = None
        v_quick = ["A", "1", "0"]
        if mode[0] == "N" and len(mode) > 3:
            if mode[2] in v_quick and mode[3]:
                quick = {
                    "row": mode[2],
                    "column": mode[3]
                }

        if mode[0] == "S" and len(mode) > 6:
            if mode[2] in v_quick and mode[3] in v_quick and mode[5] in v_quick and mode[6] in v_quick:
                quick = {
                    "sg_rows": mode[2],
                    "sg_cols": mode[3],
                    "rows": mode[5],
                    "cols": mode[6]
                }
        mode = mode[0]
        if mode == "T":
            return {
                'mode': "T",
                "input_to_index": None,
                "len_input": 2
            }
        input_method = {
            'mode': mode,
            "input_to_index": {
                "rows": {},
                "cols": {}
            },
            "index_to_output": {
                "rows": {},
                "cols": {}
            },
            "len_input": 0
        }
        if mode == "N":
            if quick is None and SudokuGame.ask_yesno("Do you want to use the default mode for this method "
                                                      "(A to I for rows, 1 to 9 for columns)? (y/n) "):
                quick = {
                    "row": "A",
                    "column": "1"
                }

            def select_n_map(quick_mode):
                if quick_mode is None:
                    quick_mode = {"row": None, "column": None}
                for pos in ["row", "column"]:
                    m = quick_mode[pos]
                    while m is None:
                        m = input(f"Please select the range of values that you want for a cell's {pos}?\n"
                                  f"0- For values from 0 to 8\n"
                                  f"1- For values from 1 to 9\n"
                                  f"A- For values from A to I\n"
                                  f"C- For custom values (that you will enter yourself)\n").upper()[0]
                        if m not in ["0", "1", "A", "C"]:
                            print("You need to select one of the options of the menu (0, 1, A or C)!\n")
                            m = None
                    if m == "0":
                        for i in range(9):
                            input_method["input_to_index"][f"{pos[0:3]}s"][str(i)] = i
                            input_method["index_to_output"][f"{pos[0:3]}s"][i] = str(i)
                    elif m == "1":
                        for i in range(1, 10):
                            input_method["input_to_index"][f"{pos[0:3]}s"][str(i)] = i-1
                            input_method["index_to_output"][f"{pos[0:3]}s"][i-1] = str(i)
                    elif m == "A":
                        for i in range(9):
                            input_method["input_to_index"][f"{pos[0:3]}s"]["ABCDEFGHI"[i]] = i
                            input_method["index_to_output"][f"{pos[0:3]}s"][i] = "ABCDEFGHI"[i]
                    elif m == "C":
                        assigned = []
                        for i in range(9):
                            duplicate = True
                            char = ""
                            while duplicate:
                                char = input(f"Enter one single character for the {i+1}nth {pos}: ").upper()[0]
                                if char in assigned:
                                    print("You cannot use the same character twice!")
                                else:
                                    duplicate = False
                            assigned.append(char)
                            input_method["input_to_index"][f"{pos[0:3]}s"][char] = i
                            input_method["index_to_output"][f"{pos[0:3]}s"][i] = char
                input_method["len_input"] = 2
                return input_method
            return select_n_map(quick)
        if mode == "S":
            if quick is None and SudokuGame.ask_yesno("Do you want to use the default mode for this method "
                                                      "(A to C for the subgrid' row, 1 to 3 for the subgrid's column"
                                                      "0 to 3 for the cell's row and column)? (y/n) "):
                quick = {
                    "sg_rows": "A",
                    "sg_cols": "A",
                    "rows": "0",
                    "cols": "0"
                }

            def select_s_map(quick_mode):
                if quick_mode is None:
                    quick_mode = {
                        "sg_rows": None,
                        "sg_cols": None,
                        "rows": None,
                        "cols": None
                    }
                input_method["input_to_index"]["sg_rows"] = dict()
                input_method["index_to_output"]["sg_rows"] = dict()
                input_method["input_to_index"]["sg_cols"] = dict()
                input_method["index_to_output"]["sg_cols"] = dict()

                for pos in ["sg_rows", "sg_cols", "rows", "cols"]:
                    m = quick_mode[pos]
                    while m is None:
                        m = input(f"Please select the range of values that you want for a "
                                  f"{'subgrid' if 'sg' in pos else 'cell'}'s {'row' if 'row' in pos else 'column'}?\n"
                                  f"0- For values from 0 to 8\n"
                                  f"1- For values from 1 to 9\n"
                                  f"A- For values from A to I\n"
                                  f"C- For custom values (that you will enter yourself)\n").upper()[0]
                        if m not in ["0", "1", "A", "C"]:
                            print("You need to select one of the options of the menu (0, 1, A or C)!\n")
                            m = None
                    if m == "0":
                        for i in range(3):
                            input_method["input_to_index"][pos][str(i)] = i
                            input_method["index_to_output"][pos][i] = str(i)
                    elif m == "1":
                        for i in range(1, 4):
                            input_method["input_to_index"][pos][str(i)] = i-1
                            input_method["index_to_output"][pos][i-1] = str(i)
                    elif m == "A":
                        for i in range(3):
                            input_method["input_to_index"][pos]["ABC"[i]] = i
                            input_method["index_to_output"][pos][i] = "ABC"[i]
                    elif m == "C":
                        assigned = []
                        for i in range(3):
                            duplicate = True
                            char = ""
                            while duplicate:
                                char = input(f"Enter one single character for the {i+1}nth "
                                             f"{'subgrid' if 'sg' in pos else 'cell'}'s "
                                             f"{'row' if 'row' in pos else 'column'}: ").upper()[0]
                                if char in assigned:
                                    print("You cannot use the same character twice!")
                                else:
                                    duplicate = False
                            assigned.append(char)
                            input_method["input_to_index"][pos][char] = i
                            input_method["index_to_output"][pos][i] = char
                input_method["len_input"] = 5
                return input_method
            return select_s_map(quick)

    def index_from_input(self, string: str) -> int:
        string = string.upper().replace(' ', '')
        itoi = self.input_method["input_to_index"]
        if self.input_method["mode"] == "T":
            return int(string[0:1 if len(string) == 1 else 2])
        if self.input_method["mode"] == "N":
            return itoi["rows"][string[0]]*9 + itoi["cols"][string[1]]
        if self.input_method["mode"] == "S":
            return (itoi["sg_rows"][string[0]]*3 + itoi["rows"][string[2]]) * 9 + \
                   itoi["sg_cols"][string[1]]*3 + itoi["cols"][string[3]]

    @staticmethod
    def pos_to_index(pos: Position = None, row: int = None, col: int = None):
        if pos is not None:
            return pos.row*9 + pos.col
        return row*9 + col

    @staticmethod
    def index_to_pos(index: int) -> Position:
        return Position(index//9, index % 9)

    def index_to_output(self, pos: Position = None, row: int = None, col: int = None, index: int = None) -> str:
        if pos is None:
            if row is not None and col is not None:
                pos = Position(row, col)
            elif index:
                pos = self.index_to_pos(index)

        itoo = self.input_method["index_to_output"]
        if self.input_method["mode"] == "N":
            return f'{itoo["rows"][pos.row]}{itoo["cols"][pos.col]}'
        if self.input_method["mode"] == "S":
            return f'{itoo["sg_rows"][pos.row//3]}{itoo["sg_cols"][pos.col//3]} ' \
                   f'{itoo["rows"][pos.row%3]}{itoo["cols"][pos.col%3]}'

    def cell_from_index(self, index) -> SudokuCell:
        return self.board[index]

    def cell_from_input(self, string: str) -> SudokuCell:
        try:
            return self.board[self.index_from_input(string)]
        except ValueError:
            print("Error: The cell index has to be a number!")
        except KeyError or IndexError:
            print("Error: Invalid cell position format or value out of range")
            self.input_info()

    def ask_user_for_index(self, message: str="") -> int:
        try:
            return self.index_from_input(input(message))
        except ValueError:
            print("Error: The cell index has to be a number!")
        except KeyError or IndexError:
            print("Error: Invalid cell position format or value out of range")
            self.input_info()

    def ask_user_for_cell(self, message: str="") -> SudokuCell:
        index = self.ask_user_for_index(message)
        if index is not None:
            return self.cell_from_index(index)

    @staticmethod
    def ask_user_for_value(message: str="") -> int:
        while True:
            try:
                return int(input(message))
            except ValueError:
                print("Error: The value has to be a number!")

    def input_info(self):
        if self.input_method["mode"] == "N":
            r = random.randint(0, 8)
            c = random.randint(0, 8)
            print(f"To the left of the grid you have the letter/number of each row, at the top you have the "
                  f"letter/number of each column, to select a cell type the row then the column.\nExample: for row "
                  f"{r+1} column {c+1} you can type {self.index_to_output(row=r, col=c)}")
        elif self.input_method["mode"] == "S":
            r = random.randint(0, 2)
            c = random.randint(0, 2)
            sg_r = random.randint(0, 2)
            sg_c = random.randint(0, 2)
            print(f"To the right of the grid you have the letter/number of each subgrid row, at the bottom you have "
                  f"the letter/number of each subgrid column. To the left you have the row of each cell within the "
                  f"subgrid and at the top you have the column of each cell within the subgrid.\nTo select a cell "
                  f"type the subgrid row and column then the row and column within the subgrid.\nExample: for row "
                  f"{sg_r*3+r+1} column {sg_c*3+c+1} which is in the row {r+1} col {c+1} in the subgrid at the row "
                  f"{sg_r+1} col {sg_c+1} you can type {self.index_to_output(row=sg_r*3+r, col=sg_c*3+c)}")
        else:
            print("To select a cell, just count the cells before it and type it, ez pz")

    def select_cell(self, cell: SudokuCell):
        self.deselect_cell()
        cell.selected = True
        self.selected_cell = cell

    def deselect_cell(self):
        if self.selected_cell is not None:
            self.selected_cell.selected = False

    def assign_value_to_selected(self, value: int):
        if 0 <= value < 10:
            if self.selected_cell.assign_value(value):
                print("Value successfully assigned")
                return True
            else:
                print("Error: You cannot assign a value to a cell generated by the game")
                return True
        else:
            print("Error: The value has to be a number between 1 and 9")
            return False

    def tutorial(self):

        input("Welcome to the tutorial! You can input values freely here before the game starts to get used to the "
              "input methods! (press enter to continue) ")

        self.show_board()
        self.input_info()

        while True:
            cell = self.ask_user_for_cell("To continue, select a cell: ")
            if cell is not None:
                break
        self.select_cell(cell)
        self.show_board()

        print("You can see the selected cell in the grid with the two arrows'> <'")

        while True:
            value = self.ask_user_for_value("Now input a value between 1 and 9: ")
            if self.assign_value_to_selected(value):
                break
        self.deselect_cell()
        self.show_board()
        input("You can now see it in the grid! (press enter to continue)")

        input("As you can see your value is now wrapped within two square brackets, this differentiates it from the "
              "locked values of the grid that are not wrapped with anything. Let's add another value to the grid! "
              "(press enter to continue)")

        cell = self.board[random.randint(0, 80)]
        cell.assign_static_value(random.randint(1, 9))
        self.show_board()

        input("If you select a cell that is set by the game (press enter to continue)")

        self.select_cell(cell)
        self.show_board()

        value = self.ask_user_for_value("And try to assign a value to it (enter a value to continue)")

        print("You will get an error like this: ")
        self.assign_value_to_selected(value)
        time.sleep(1)

        print(f"You can also quickly input a position and a value at once by typing the cell's position and it's value"
              f" separated by a space like this: {self.random_pos_with_value()}"
              f"\nNow try it: ")
        while True:
            if self.quick_pos_value_input(input()):
                break
        self.show_board()
        print('You can now play freely with the tutorial\'s grid, select other cells and assign other values.\n'
              'Once you are done you can type "/verify" to verify if you won. You can also type "/quit" or "/exit" '
              'to leave this tutorial\n')
        if self.input_loop(True):
            print("You didn't really have to win the tutorial lol")
        print("Now let's go back to the real deal!")

    def random_pos_with_value(self):
        return f"{self.index_to_output(index=random.randint(0, 80))} {random.randint(1, 9)}"

    def random_pos(self):
        return f"{self.index_to_output(index=random.randint(0, 80))}"

    def quick_pos_value_input(self, inp: str) -> bool:
        len_pos = self.input_method["len_input"]
        cell = self.cell_from_input(inp[0:len_pos])
        if cell is None:
            return False
        try:
            value = int(inp[len_pos + 1:len_pos + 3])
        except ValueError:
            print("What comes after the position can only be a number representing the value that you want to"
                  "assign to the cell for quick assignment")
            return False
        self.select_cell(cell)
        return self.assign_value_to_selected(value)

    def help(self):
        print(f"You can select a cell by typing it's position like this: \"{self.random_pos()}\" \n"
              f"Or assign a value to a cell by typing a cell's position followed by it's value (with a space "
              f"inbetween) like this: \"{self.random_pos_with_value()}\"\nAssigning a value of 0 clears the cell!\n"
              f"The commands you can use are: \n"
              f"/verify to verify the grid and check if you won\n"
              f"/quit or /exit to close the game\n"
              f"/help to show this menu")
        if self.ask_yesno("Do you want to see the cell position input help?"):
            self.input_info()

    def input_loop(self, tutorial=False):
        while True:
            inp = input()
            if "help" in inp:
                self.help()
                continue
            if inp.startswith("/"):
                if "quit" in inp or "exit" in inp:
                    if tutorial or self.ask_yesno("Do you really want to leave? (y/n) "):
                        return False
                if "verify" in inp:
                    if self.board.has_empty_cells():
                        print("The grid still has empty cells!")
                        continue
                    elif self.board.has_duplicates():
                        print("The grid contains duplicate values!")
                        continue
                    else:
                        return True
            len_pos = self.input_method["len_input"]
            if len(inp) >= len_pos + 2:
                if self.quick_pos_value_input(inp):
                    self.show_board()
                continue
            else:
                cell = self.cell_from_input(inp)
                if cell is None:
                    continue
                while True:
                    value = self.ask_user_for_value(f"Enter the value for the {self.index_to_output(pos=cell.pos)} "
                                                    f"cell:")
                    self.select_cell(cell)
                    if self.assign_value_to_selected(value):
                        break
                self.show_board()

    def start_game(self, skip_tutorial=False):
        self.new_board()
        if self.input_method["mode"] is None:
            self.input_method = self.set_input_method()
        if not skip_tutorial:
            if self.ask_yesno("Do you want to run the tutorial? (yes/no) "):
                self.tutorial()
            else:
                self.input_info()

        if self.difficulty is None:      # only used on first start restarting will ask it by itself
            self.difficulty = self.ask_difficulty()
        # TODO add hard+ difficulty specificity where user has only one win verification
        if self.track_time is None:
            self.track_time = self.ask_yesno("Would you like to track your time? (y/n): ")
        # DONE ask for their favourite input method, we'll take the example being the 4th row 5th column cell:
        # (A to I)(1 to 9) ex: D5   (can also work with 0 to 8 ex: D4)
        # (1 to 9)(1 to 9) ex: 45   (can also work with 0 to 8 ex: 34)
        # (0 to 2)(0 to 2) (0 to 2)(0 to 2) ex: 11 01  (can also work with 1 to 3)
        # (A to C)(A to C) (0 to 2)(0 to 2) ex: BB 01  (can also work with 1 to 3)
        # the selection would ask if they would rather pick the first 9*9 grid or use subgrids, then ask whether they
        # prefer numbers or letters for each variable and set a decryption dictionary from that, they could also input
        # their own dictionary which would map a set of keys in a set position to the value
        print("\nHere's a fresh new grid:")
        self.new_board()
        self.show_board()
        print("Generating a fully playable grid with a unique solution...")
        self.make_board_playable()
        if self.track_time:
            while True:
                if self.ask_yesno("Are you ready? Time will be tracked once you are (y/n) "):
                    break
                elif self.ask_yesno("Do you need help? (y/n means that you can answer with yes or no)"):
                    self.help()
                else:
                    print("Take your time then and only answer when you are!")
        self.show_board()
        if self.track_time:
            self.time_beginning = time.time()
        if self.input_loop():
            if self.track_time:
                print(f"Congratulations! "
                      f"The grid has been successfully completed in {format_time(time.time() - self.time_beginning)}!")
            else:
                print("Congratulations! The grid has been successfully completed!")
            if self.ask_yesno("Do you want to restart the game? (y/n) "):
                if not self.ask_yesno("Do you want to keep the current input settings? (y/n) "):
                    self.input_method["mode"] = None
                if not self.ask_yesno("Do you want to keep the current difficulty and time tracking settings? (y/n) "):
                    self.difficulty = None
                    self.track_time = None
                    self.time_beginning = None

                self.start_game(True)
        print("Goodbye then!")

        # TODO add save mechanisms that store the state of the game in a json file and add a json unpacker
        # TODO the possibilities are infinite, the only limit is your imagination

